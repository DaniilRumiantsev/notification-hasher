# Notification Hasher
Receives push notifications and searches for 'toHash' field in data type messages.
Extracts data from 'toHash' and hashes it via SHA-256 algorithm implemented in C++.
## Sending notifications
To send a notification you need a client-side token (which is displayed when you launch the app for the first time on the client and printed into LogCat every time you launch the app) and a server-side key (provided personnally).
### Example of sending a notification
This is how you can test sending and receiving of notifications via curl.

curl -i -H 'Content-type: application/json' -H 'Authorization: key=<server-key>' -XPOST https://fcm.googleapis.com/fcm/send -d '{
  "registration_ids":["<client-token-1>", "<client-token-2>", ...],
  "data": {
    "toHash" : "message text"
  }
}'
