#include "sha256.h"

uint32_t native::sha256::round_keys[64] = {
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

uint32_t native::sha256::rotate_right(uint32_t value, int n)
{
    return ((value >> n) | (value << (32-n))) & 0xffffffff;
}

uint32_t native::sha256::Ch(uint32_t E, uint32_t F, uint32_t G)
{
    return (E&F) ^ ((~E)&G);
}

uint32_t native::sha256::Ma(uint32_t A, uint32_t B, uint32_t C)
{
    return (A&B) ^ (A&C) ^ (B&C);
}

uint32_t native::sha256::E0(uint32_t A)
{
    return rotate_right(A, 2) ^ rotate_right(A, 13) ^ rotate_right(A, 22);
}

uint32_t native::sha256::E1(uint32_t E)
{
    return rotate_right(E, 6) ^ rotate_right(E, 11) ^ rotate_right(E, 25);
}

uint32_t native::sha256::S0(uint32_t value)
{
    return rotate_right(value, 7) ^ rotate_right(value, 18) ^ (value >> 3);
}

uint32_t native::sha256::S1(uint32_t value)
{
    return rotate_right(value, 17) ^ rotate_right(value, 19) ^ (value >> 10);
}

template<typename T>
void native::sha256::append_as_big_endian(unsigned char *dest, T value)
{
    for (size_t i = 0; i < sizeof(T); ++i)
    {
        *dest++ = static_cast<unsigned char>((value >> 8*(sizeof(T)-1-i))&0xFF);
    }
}

uint32_t native::sha256::chars_as_big_endian(unsigned char *source)
{
    return static_cast<uint32_t>((*source << 24) |
                                 (*(source+1) << 16) |
                                 (*(source+2) << 8) |
                                 *(source+3));

}

/*
    Pre-processing (Padding):
    begin with the original message of length L bits
    append a single '1' bit
    append K '0' bits, where K is the minimum number >= 0 such that L + 1 + K + 64 is a multiple of 512
    append L as a 64-bit big-endian integer, making the total post-processed length a multiple of 512 bits
*/
void native::sha256::padding(const unsigned char *text, uint64_t size)
{
    // Convert size in bytes to bits to get L
    uint64_t L = size << 3;
    // Calculate K
    uint64_t K = 512 - (1+64+L)%512;
    // Calculate size of padded message in bytes
    padded_size = (L+1+K+64)/8;
    // Ensure enough memory for padded message
    message = new unsigned char[padded_size];
    // Copy initial text to newly initialized memory
    memcpy(message, text, size);
    // Append single '1' bit and 7 '0' bits, so there are K-7 '0' bits left to add
    *(message+size) = static_cast<unsigned char>(0x80);
    // Append K-7 '0' bits. K-7 is guaranteed to be multiple to 8
    memset(message+size+1, 0, (K-7)/8);
    // Append L as big-endian
    append_as_big_endian(message+(padded_size-8), L);
}

void native::sha256::calculate_hash()
{
    // Split padded message into 512-bit (i.e. 64-byte) chunks
    uint64_t chunks_quantity = padded_size/64;
    // Perform actions with each chunk
    for (size_t i = 0; i < chunks_quantity; ++i)
    {
        handle_chunk(message+i*64);
    }
    // Calculate final hash value from collected results
    for (size_t i = 0; i < 8; ++i)
    {
        append_as_big_endian(hash+(sizeof(uint32_t)*i), initial_digest[i]);
    }
}

void native::sha256::handle_chunk(unsigned char* chunk_ptr)
{
    uint32_t w[64];
    // w[0] .. w[15] initialized with bytes from chunk following in big endian order
    for (size_t i = 0; i < 16; ++i)
    {
        w[i] = chars_as_big_endian(chunk_ptr);
        chunk_ptr += 4;
    }
    // w[16] .. w[63] are calculated according to SHA256 algorithm
    for (size_t i = 16; i < 64; ++i)
    {
        w[i] = 	w[i-16] + S0(w[i-15]) + w[i-7] + S1(w[i-2]);
    }

    uint32_t a = initial_digest[0];
    uint32_t b = initial_digest[1];
    uint32_t c = initial_digest[2];
    uint32_t d = initial_digest[3];
    uint32_t e = initial_digest[4];
    uint32_t f = initial_digest[5];
    uint32_t g = initial_digest[6];
    uint32_t h = initial_digest[7];

    // Main cycle
    for (int i = 0; i < 64; ++i)
    {
        uint32_t temp1 = h + E1(e) + Ch(e,f,g) + round_keys[i] + w[i];
        uint32_t temp2 = E0(a) + Ma(a,b,c);

        h = g;
        g = f;
        f = e;
        e = d + temp1;
        d = c;
        c = b;
        b = a;
        a = temp1 + temp2;
    }

    initial_digest[0] += a;
    initial_digest[1] += b;
    initial_digest[2] += c;
    initial_digest[3] += d;
    initial_digest[4] += e;
    initial_digest[5] += f;
    initial_digest[6] += g;
    initial_digest[7] += h;
}

native::sha256::sha256(const unsigned char* text, uint64_t size)
{
    // Pad given message
    padding(text, size);
    // Calculate hash and save it to 'hash' member
    calculate_hash();
}

native::sha256::~sha256()
{
    delete[] message;
}

void native::sha256::operator()(unsigned char* buffer)
{
    memcpy(buffer, hash, 32);
}

extern "C" JNIEXPORT jbyteArray JNICALL Java_com_daniilrumyantsev_notificationhasher_utils_NotificationService_getHash(
    JNIEnv* env,
    jobject,
    jbyteArray text
)
{
    // Copy Java byte[] array into array of unsigned chars pointed by buffer
    int text_length = env->GetArrayLength(text);
    unsigned char* buffer = new unsigned char[text_length];
    env->GetByteArrayRegion(text, 0, text_length, reinterpret_cast<jbyte*>(buffer));

    // Calculate hash and free the buffer
    unsigned char hash[32];
    native::sha256(buffer, text_length)(hash);
    delete[] buffer;

    // Return hash not as unsigned char[32] but as Java byte[32]
    jbyteArray array = env->NewByteArray(32);
    env->SetByteArrayRegion(array, 0, 32, reinterpret_cast<jbyte*>(hash));
    return array;
}
