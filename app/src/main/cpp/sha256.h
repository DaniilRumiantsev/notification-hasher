#ifndef SHA256_H
#define SHA256_H

#include <cstdint>
#include <cstring>
#include <jni.h>

namespace native
{

    class sha256
    {
        // Standard initial digest and round keys used in SHA256
        uint32_t initial_digest[8] = {
                0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
                0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
        };
        static uint32_t round_keys[64];

        // Bitwise operations & standard round conversions
        static inline uint32_t rotate_right(uint32_t value, int n);
        static inline uint32_t Ch(uint32_t E, uint32_t F, uint32_t G);
        static inline uint32_t Ma(uint32_t A, uint32_t B, uint32_t C);
        static inline uint32_t E0(uint32_t A);
        static inline uint32_t E1(uint32_t E);
        static inline uint32_t S0(uint32_t value);
        static inline uint32_t S1(uint32_t value);

        // Read integral value and append to dest starting from most significant byte
        template<typename T>
        static void append_as_big_endian(unsigned char* dest, T value);

        // Interpret 4 bytes starting from source as uint32_t value
        static inline uint32_t chars_as_big_endian(unsigned char* source);

        // Padded message
        unsigned char* message;
        // Size of padded message in bytes
        uint64_t padded_size;
        // Value of hash
        unsigned char hash[32];

        // Pad the message so that in can be split into 512-bit chunks
        void padding(const unsigned char* text, uint64_t size);
        // Split padded message into chunks, call handle_chunk() for each chunk,
        // collect results into hash variable
        void calculate_hash();
        // Perform actions with each 512-bit chunk according to SHA256 algorithm
        void handle_chunk(unsigned char* chunk_ptr);

    public:

        sha256(const unsigned char* text, uint64_t size);
        ~sha256();
        // We don't support copy or move semantics
        sha256(sha256& other) = delete;
        sha256(sha256&& tmp) = delete;
        // Puts value of hash to given buffer
        void operator()(unsigned char* buffer);
    };

}

#endif