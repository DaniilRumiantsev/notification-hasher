package com.daniilrumyantsev.notificationhasher;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daniilrumyantsev.notificationhasher.database.DbManager;
import com.daniilrumyantsev.notificationhasher.model.HashEntity;
import com.daniilrumyantsev.notificationhasher.utils.NotificationService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.List;

public class MainActivity extends AppCompatActivity
{
    static
    {
        System.loadLibrary("native-sha256");
    }

    private final static String PREF_INITIALIZED = "com.daniilrumyantsev.notificationhasher.init";

    private RecyclerView mRecyclerView;
    private EntityAdapter mAdapter;

    @Override
    protected void onStart()
    {
        super.onStart();

        // Initialize firebase app, put current token to LogCat ans show it in dialog
        // if the app is launched for the first time
        logAndShowFirebaseToken();

        // Register dynamic receiver which will capture notifications and hide them,
        // so that notifications are not shown then app is active
        registerReceiver(
                mOnShowNotification,
                new IntentFilter(NotificationService.ACTION_SHOW_NOTIFICATION),
                NotificationService.PERM_PRIVATE,
                null
        );

        // If there is notification in the bar, remove it
        NotificationManager notificationManager =
                (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        unregisterReceiver(mOnShowNotification);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.entities_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        updateViewAndDatabase();
    }

    private void logAndShowFirebaseToken()
    {
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>()
                {
                    @Override
                    public void onComplete(Task<InstanceIdResult> task)
                    {
                        if (!task.isSuccessful())
                        {
                            Log.d("FirebaseToken", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        // Log token
                        Log.d("FirebaseToken", token);
                        // Show greeting message with token
                        showGreetingIfLaunchFirstTime(token);
                    }
                });
    }

    private void updateViewAndDatabase()
    {
        List<HashEntity> entities = DbManager.get(this).getHashEntities(null,null);

        if (mAdapter == null)
        {
            mAdapter = new EntityAdapter(entities);
            mRecyclerView.setAdapter(mAdapter);
        }
        else
        {
            mAdapter.setEntities(entities);
            mAdapter.notifyDataSetChanged();
        }

        DbManager.get(this).setAllRead();
    }

    private void showGreetingIfLaunchFirstTime(String token)
    {
        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
        boolean alreadyInitialized = pref.getBoolean(PREF_INITIALIZED, false);
        if (alreadyInitialized) return;

        pref.edit().putBoolean(PREF_INITIALIZED, true).apply();

        Resources resources = getResources();
        String message = resources.getString(R.string.nothing_to_show)+
                "\n"+resources.getString(R.string.your_app_token)+
                "\n"+token;
        TextView messageView = new TextView(this);
        messageView.setText(message);
        messageView.setTextIsSelectable(true);
        messageView.setPadding(20,20,20,20);

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(R.string.welcome);
        alertDialog.setView(messageView);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private BroadcastReceiver mOnShowNotification = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (isOrderedBroadcast())
            {
                setResultCode(RESULT_CANCELED);
            }
            updateViewAndDatabase();
        }
    };

    private class EntityHolder extends RecyclerView.ViewHolder
    {
        private HashEntity mHashEntity;
        private TextView mDateView;
        private TextView mMessageView;
        private TextView mHashView;

        public EntityHolder(LayoutInflater inflater, ViewGroup parent)
        {
            super(inflater.inflate(R.layout.list_entity, parent, false));
            mDateView = itemView.findViewById(R.id.entity_date);
            mMessageView = itemView.findViewById(R.id.entity_message);
            mHashView = itemView.findViewById(R.id.entity_hash);
        }

        public void bind(HashEntity he)
        {
            mHashEntity = he;
            mDateView.setText(mHashEntity.getFormattedDate());
            mMessageView.setText(mHashEntity.getMessage());
            mHashView.setText(mHashEntity.getReadableHash());

            if (!mHashEntity.isRead())
            {
                mDateView.setTypeface(null, Typeface.BOLD);
                mMessageView.setTypeface(null, Typeface.BOLD);
                mHashView.setTypeface(null, Typeface.BOLD);
            }
        }
    }

    private class EntityAdapter extends RecyclerView.Adapter<EntityHolder>
    {
        private List<HashEntity> mEntities;

        public EntityAdapter(List<HashEntity> entities)
        {
            mEntities = entities;
        }

        @Override
        public EntityHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            return new EntityHolder(LayoutInflater.from(MainActivity.this), parent);
        }

        @Override
        public void onBindViewHolder(EntityHolder holder, int position)
        {
            holder.bind(mEntities.get(position));
        }

        @Override
        public int getItemCount()
        {
            return mEntities.size();
        }

        public void setEntities(List<HashEntity> entities)
        {
            mEntities = entities;
        }
    }
}
