package com.daniilrumyantsev.notificationhasher.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.daniilrumyantsev.notificationhasher.model.HashEntity;

public class DbCursor extends CursorWrapper
{
    DbCursor(Cursor c)
    {
        super(c);
    }

    public HashEntity getHashEntity()
    {
        HashEntity he = new HashEntity();
        he.setDate(getLong(getColumnIndex(DbSchema.HashesTable.Cols.DATE_TIME)));
        he.setMessage(getString(getColumnIndex(DbSchema.HashesTable.Cols.MESSAGE)));
        he.setHash(getBlob(getColumnIndex(DbSchema.HashesTable.Cols.HASH)));
        he.setRead(getInt(getColumnIndex(DbSchema.HashesTable.Cols.READ)) == 1);
        return he;
    }
}
