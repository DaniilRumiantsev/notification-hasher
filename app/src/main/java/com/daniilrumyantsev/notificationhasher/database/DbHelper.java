package com.daniilrumyantsev.notificationhasher.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper
{
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "HashesBase.db";

    public DbHelper(Context c)
    {
        super(c, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("create table " + DbSchema.HashesTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                DbSchema.HashesTable.Cols.DATE_TIME + ", " +
                DbSchema.HashesTable.Cols.MESSAGE + ", " +
                DbSchema.HashesTable.Cols.HASH + ", " +
                DbSchema.HashesTable.Cols.READ +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
}
