package com.daniilrumyantsev.notificationhasher.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.daniilrumyantsev.notificationhasher.model.HashEntity;

import java.util.ArrayList;
import java.util.List;

public class DbManager
{
    private static DbManager sManager;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static DbManager get(Context context)
    {
        if (sManager == null)
        {
            sManager = new DbManager(context);
        }
        return sManager;
    }

    private DbManager(Context context)
    {
        mContext = context.getApplicationContext();
        mDatabase = new DbHelper(mContext).getWritableDatabase();
    }

    public void insert(HashEntity he)
    {
        ContentValues values = getContentValues(he);
        mDatabase.insert(DbSchema.HashesTable.NAME, null, values);
    }

    public List<HashEntity> getHashEntities(String where, String[] args)
    {
        List<HashEntity> entities = new ArrayList<>();
        DbCursor cursor = queryHashEntities(where, args);
        try
        {
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                entities.add(cursor.getHashEntity());
                cursor.moveToNext();
            }
        }
        finally
        {
            cursor.close();
        }
        return entities;
    }

    public void setAllRead()
    {
        ContentValues cv = new ContentValues();
        cv.put(DbSchema.HashesTable.Cols.READ, 1);
        mDatabase.update(DbSchema.HashesTable.NAME, cv, null, null);
    }

    public int countUnread()
    {
        int count = 0;
                Cursor c = mDatabase.rawQuery(
                "select count(*) from "+
                DbSchema.HashesTable.NAME+
                " where "+
                DbSchema.HashesTable.Cols.READ+
                " = 0",
                null
        );
        if (c.moveToFirst()) count = c.getInt(0);
        c.close();
        return count;
    }

    private DbCursor queryHashEntities(String whereClause, String[] whereArgs)
    {
        Cursor cursor = mDatabase.query(
                DbSchema.HashesTable.NAME,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                "_id desc",  // orderBy
                "1000" // limit
        );
        return new DbCursor(cursor);
    }

    private static ContentValues getContentValues(HashEntity he)
    {
        ContentValues values = new ContentValues();
        values.put(DbSchema.HashesTable.Cols.DATE_TIME, he.getDate());
        values.put(DbSchema.HashesTable.Cols.MESSAGE, he.getMessage());
        values.put(DbSchema.HashesTable.Cols.HASH, he.getHash());
        values.put(DbSchema.HashesTable.Cols.READ, he.isRead()?1:0);
        return values;
    }
}
