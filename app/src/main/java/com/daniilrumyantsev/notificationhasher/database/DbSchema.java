package com.daniilrumyantsev.notificationhasher.database;

public class DbSchema
{
    public static final class HashesTable
    {
        public static final String NAME = "hashes";
        public static final class Cols
        {
            public static final String DATE_TIME = "date_time";
            public static final String MESSAGE = "message";
            public static final String HASH = "hash";
            public static final String READ = "read";
        }
    }
}
