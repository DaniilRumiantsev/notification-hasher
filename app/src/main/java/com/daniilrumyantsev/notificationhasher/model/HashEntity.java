package com.daniilrumyantsev.notificationhasher.model;

import java.util.Calendar;
import java.util.Formatter;

public class HashEntity
{
    private long mDate;
    private String mMessage;
    private byte[] mHash;
    private boolean mRead;

    public long getDate()
    {
        return mDate;
    }

    public String getMessage()
    {
        return mMessage;
    }

    public byte[] getHash()
    {
        return mHash;
    }

    public boolean isRead()
    {
        return mRead;
    }

    public void setDate(long date)
    {
        mDate = date;
    }

    public void setMessage(String message)
    {
        mMessage = message;
    }

    public void setHash(byte[] hash)
    {
        mHash = hash;
    }

    public void setRead(boolean read)
    {
        mRead = read;
    }

    public String getFormattedDate()
    {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(mDate);
        return new Formatter().format("%tA, %<td.%<tm.%<tY | %<tH:%<tM:%<tS", c).toString();
    }

    public String getReadableHash()
    {
        String res = new String();
        for (byte b : mHash)
        {
            String s = Integer.toHexString(b & 0xFF);
            if (s.length() == 1) res += '0';
            res += s;
        }
        return res;
    }
}
