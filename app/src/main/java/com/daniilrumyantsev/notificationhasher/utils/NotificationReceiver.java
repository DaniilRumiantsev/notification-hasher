package com.daniilrumyantsev.notificationhasher.utils;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.daniilrumyantsev.notificationhasher.R;

public class NotificationReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (getResultCode() == Activity.RESULT_OK)
        {
            int requestCode = intent.getIntExtra(NotificationService.REQUEST_CODE, 0);
            Notification notification = intent.getParcelableExtra(NotificationService.NOTIFICATION);

            if (notification == null) return;

            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                NotificationChannel channel = new NotificationChannel(
                        context.getString(R.string.default_notification_channel_id),
                        context.getString(R.string.default_notification_channel),
                        NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }

            notificationManager.notify(requestCode, notification);
        }
    }
}
