package com.daniilrumyantsev.notificationhasher.utils;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.daniilrumyantsev.notificationhasher.MainActivity;
import com.daniilrumyantsev.notificationhasher.R;
import com.daniilrumyantsev.notificationhasher.database.DbManager;
import com.daniilrumyantsev.notificationhasher.model.HashEntity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Date;

public class NotificationService extends FirebaseMessagingService
{
    private static final String TAG = "NotificationService";
    public static final String ACTION_SHOW_NOTIFICATION = "com.daniilrumyantsev.notificationhasher.SHOW_NOTIFICATION";
    public static final String PERM_PRIVATE = "com.daniilrumyantsev.notificationhasher.PRIVATE";
    public static final String REQUEST_CODE = "REQUEST_CODE";
    public static final String NOTIFICATION = "NOTIFICATION";

    public native byte[] getHash(byte[] text);

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        if (remoteMessage.getData().size() > 0)
        {
            String data = remoteMessage.getData().get("toHash");
            if (data != null)
            {
                sendBroadcast(new Intent(ACTION_SHOW_NOTIFICATION), PERM_PRIVATE);
                sendNotification();
                updateDatabase(data);
            }
        }
    }

    @Override
    public void onNewToken(String token)
    {
        Log.d(TAG, "New token: " + token);
    }

    private void sendNotification()
    {
        Resources resources = getResources();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        int newEntities = DbManager.get(this).countUnread() + 1;

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification notification =
                new NotificationCompat.Builder(
                        this,
                        resources.getString(R.string.default_notification_channel_id))
                        .setSmallIcon(android.R.drawable.ic_dialog_info)
                        .setContentTitle(resources.getString(R.string.new_data_received))
                        .setContentText(
                                resources.getQuantityString(R.plurals.calculations_performed,
                                newEntities,
                                newEntities
                                ))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .build();

        showBackgroundNotification(0, notification);
    }

    private void showBackgroundNotification(int requestCode, Notification notification)
    {
        Intent i = new Intent(ACTION_SHOW_NOTIFICATION);
        i.putExtra(REQUEST_CODE, requestCode);
        i.putExtra(NOTIFICATION, notification);
        sendOrderedBroadcast(i, PERM_PRIVATE, null, null, Activity.RESULT_OK, null, null);
    }

    private void updateDatabase(String message)
    {
        HashEntity he = new HashEntity();
        he.setDate(new Date().getTime());
        he.setMessage(message);
        he.setHash(getHash(message.getBytes()));
        he.setRead(false);
        DbManager.get(this).insert(he);
    }
}
